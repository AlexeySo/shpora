package com.example.shpora1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class material extends AppCompatActivity {
    private String text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material);
        TextView textView = findViewById(R.id.textView);
        Intent intent = getIntent();
        ConstraintLayout background = findViewById(R.id.back);
        background.setBackgroundColor(ContextCompat.getColor(this,R.color.back));
        text = intent.getStringExtra("key");
        textView.setText(Html.fromHtml(text));

    }
}
