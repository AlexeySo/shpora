package com.example.shpora1;


import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView list;
    private static Context context;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Intent intent = new Intent(this, material.class) ;
        final Activity activity = new Activity();
        list= findViewById(R.id.list);
        final String[] typesOfProgramming = getResources().getStringArray(R.array.typesOfProgramming);
        ArrayAdapter<String> items = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,typesOfProgramming);
        list.setAdapter(items);
        String s =" " ;

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                String nameFile = ((TextView)itemClicked).getText() + ".txt";
                intent.putExtra("key",getStringFromInputStream(nameFile));
                startActivity(intent);
            }

        });

    }

    private String getStringFromInputStream(String name) {
        AssetManager assetManager = this.getAssets();
        String line = "";
        String data = "";

        try {
            InputStreamReader is = new InputStreamReader(assetManager.open(name));
            BufferedReader in = new BufferedReader(is);
            while ((line = in.readLine()) != null) {
                data = data+line;
            }
            in.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
}
